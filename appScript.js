const App = {
    data() {
        return  {
            title: 'menu',
            categories: 'breakfast',
            checkedValue: '',
            breakfastArr: [],
            categoriesBreakfast: [ 'Яйца', 'Завтраки', 'Каши', 'Круассаны', 'Блинчики', 'Десерты'],
            categoriesMenu: [ 'Закуски', 'Салаты', 'Супы', 'Паста', 'Горячее', 'Гарниры', 'Пицца', 'Тесто', 'Десерты', 'Круассаны'],
            categoriesBar: [ 'Кофе', 'Чай', 'Лимонады'],
            displayArr: [
                { "categories": "Яйца", "price": "460.-", "title": "Шакшука со страчателлой и миксом салата в соусе из сладких томатов" },
                { "categories": "Яйца", "price": "600.-", "title": "Пашот с лососем, сладким томатом, огурцами и миксом салата" },
                { "categories": "Яйца", "price": "450.-", "title": "Пашот с креветками, сладким томатом и миксом салата" },
                { "categories": "Яйца", "price": "420.-", "title": "Пашот в томатном соусе с таджаскими оливками" },
                { "categories": "Яйца", "price": "320.-", "title": "Омлет со сладким томатом и миксом салата" },
                { "categories": "Яйца", "price": "370.-", "title": "Омлет с беконом, сладким томатом и миксом салата" },
                { "categories": "Яйца", "price": "490.-", "title": "Омлет с креветками, сладким томатом и миксом салата" },
                { "categories": "Яйца", "price": "550.-", "title": "Омлет с лососем, сладким томатом и миксом салата" },
                { "categories": "Яйца", "price": "440.-", "title": "Скрембл с креветками" },
                { "categories": "Яйца", "price": "270.-", "title": "Скрембл" },
                { "categories": "Яйца", "price": "310.-", "title": "Глазунья с беконом" },
                { "categories": "Яйца", "price": "240.-", "title": "Глазунья" },
                { "categories": "Яйца", "price": "80.-", "title": "Яйцо отварное" }
            ],
            photoArr: [],
            src: 'https://thumb.tildacdn.com/tild3764-3264-4336-b132-313566633662/-/resize/1000x1600/-/format/webp/------.jpg'
        }
    },
    mounted: 
        function (){
            this.fetchBreakfast()
            this.fetchPhotos()
        },
    methods: {
        async fetchBreakfast() {
            const { data } = await axios.get('https://vue-with-http-e0075-default-rtdb.firebaseio.com/breakfast.json')
            const result = Object.keys(data).map(key => {
                return {
                    id: key,
                    ...data[key]
                }
            })
            this.breakfastArr = result
        },
        async fetchPhotos() {
            const { data } = await axios.get('https://vue-with-http-e0075-default-rtdb.firebaseio.com/photo.json')
            const result = Object.keys(data).map(key => {
                return {
                    id: key,
                    ...data[key]
                }
            })
            this.photoArr = result
        },
        showBreakfast () {
            this.categories = 'breakfast'
        },
        showMenu () {
            this.categories = 'menu'
        },
        showBar () {
            this.categories = 'bar'
        },
        displayCategory () {
            let category = event.target.innerHTML
            this.displayArr = this.breakfastArr.filter(e => e.categories === category)
            let rightPhoto = this.photoArr.find(e => e.category === category)
            this.src = rightPhoto.src
        }
    },
    computed: {
        activeBtn () {
            return 'btn-active'
        }
    }
}
Vue.createApp(App).mount('#app')