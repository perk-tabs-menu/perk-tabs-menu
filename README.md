# perk--tabs-menu

<p>Our project is a dynamic website menu for a gastrobistro, developed using <strong>Vue.js</strong> and <strong>JavaScript</strong>. We utilized <strong>Firebase</strong>, a cloud-based data storage service provided by Google, as our backend storage solution. </p>

<p>The main challenge we faced was integrating our JavaScript code with the Tilda constructor, which posed certain limitations. However, we successfully overcame these obstacles to create a user-friendly and visually appealing website menu that showcases the delightful offerings of our gastrobistro.</p>

<p>With seamless navigation and real-time updates, customers can easily browse through our menu items and make informed choices. Our Vue.js implementation ensures a smooth and interactive user experience, while Firebase handles the storage and retrieval of menu data, ensuring reliability and scalability.</p>

<p><i>Explore our website menu and discover the delectable culinary delights that await you at our gastrobistro!</i></p>

<hr>

<h4>Thanks for attention!</h4>
